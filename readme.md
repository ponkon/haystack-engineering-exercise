# Haystack Engineering Excersize

This exercise created a simple API that interfaces with an openmrs database.

## /encounters `GET`

Returns an array of all encounters. Each encounter has the following shape:

```json
{
    "id": 48844,
    "date": "2006-10-10 00:00:00.0",
    "patientId": 5587
}
```

## /encounters/:encounterId/diagnosis `GET`

Returns the diagnosis for a specific `encounterId` in plain text.

## /patients/search `POST`

Returns a list of patient ids for query supplied as json in the post body. A search consists of:

|Query Parameter | Notes|
|---- | -------|
|key | The lab result key that is being queried|
|value | The value used for the key query|
|operator | The boolean operator to be used in the query|

### Currently supported queries

#### key
 * `cd4`

#### value
 * any number

#### operator
 * `<`

### Example curl request

#### Request
```bash
curl -X POST -H "Content-Type: application/json" -d '{"key":"cd4","value":10,"operator":"<"}' 'http://localhost:8000/patients/search'
```

#### Response
```bash
["MRN001362","MRN000914","MRN008788","MRN002665","MRN000391","MRN003447","MRN005826"]
```