function handleSearchLabResultsLoad () {
  var resultsElement = document.getElementById('results');
  try {
    patients = JSON.parse(this.response);
    
    var htmlBuildup = '<table class="lab-results"><thead><tr><th>Patient ID</th></tr></thead><tbody>';
    var tableRows = _.map(patients, function(patientMRN) {
      return '<tr><td>'+patientMRN+'</td></tr>';
    });
    htmlBuildup = htmlBuildup + tableRows.join('');
    htmlBuildup = htmlBuildup + '</tbody></table>';
    resultsElement.innerHTML = htmlBuildup;
  } catch (e) {
    resultsElement.innerHTML = this.response;
  }
}

function handleSearchLabResultsSubmit(e) {
  e.preventDefault();
  var searchKey = document.getElementById('search-lab-results-key').value;
  var searchOperator = document.getElementById('search-lab-results-operator').value;
  var searchValue = document.getElementById('search-lab-results-value').value;

  var reqData = {
    key: searchKey,
    operator: searchOperator,
    value: searchValue
  }

  var newXHR = new XMLHttpRequest();
  newXHR.addEventListener('load', handleSearchLabResultsLoad);
  newXHR.open('POST', '/patients/search');
  newXHR.setRequestHeader('Content-Type', 'application/json');
  newXHR.send(JSON.stringify(reqData));
}

function handleGetEncountersLoad () {
  var resultsElement = document.getElementById('results');
  try {
    encounters = JSON.parse(this.response);
    
    var htmlBuildup = '<table class="encounters"><thead><tr><th>Encounter ID</th><th>Encounter Date</th><th>Patient ID</th></tr></thead><tbody>';
    var tableRows = _.map(encounters, function(encounter) {
      var encounterDate = new Date(encounter.date)
      return '<tr><td>'+encounter.id+'</td><td>'
        +encounter.date.substring(0,10)+'</td><td>'
        +encounter.patientId+'</td></tr>';
    });
    htmlBuildup = htmlBuildup + tableRows.join('');
    htmlBuildup = htmlBuildup + '</tbody></table>';
    resultsElement.innerHTML = htmlBuildup;
  } catch (e) {
    resultsElement.innerHTML = this.response;
  }
}

function handleGetEncountersSubmit(e) {
  e.preventDefault();

  var newXHR = new XMLHttpRequest();
  newXHR.addEventListener('load', handleGetEncountersLoad);
  newXHR.open('GET', '/encounters');
  newXHR.send();
}

function handleFindDiagnosisLoad () {
  var resultsElement = document.getElementById('results');
  try {
    resultsElement.innerHTML = '<p class="single-value-result">Diagnosis: '+this.response+'</p>';
  } catch (e) {
    resultsElement.innerHTML = this.response;
  }
}

function handleFindDiagnosisSubmit(e) {
  e.preventDefault();
  var encounterId = document.getElementById('find-diagnosis-encounter-id').value;

  var newXHR = new XMLHttpRequest();
  newXHR.addEventListener('load', handleFindDiagnosisLoad);
  newXHR.open('GET', '/encounters/'+encounterId+'/diagnosis');
  newXHR.send();
}

// Setup submit handlers
var searchLabResultsForm = document.getElementById("search-lab-results");
var getEncountersForm = document.getElementById("get-all-encounters");
var findDiagnosisForm = document.getElementById("find-diagnosis");

searchLabResultsForm.onsubmit = handleSearchLabResultsSubmit;
getEncountersForm.onsubmit = handleGetEncountersSubmit;
findDiagnosisForm.onsubmit = handleFindDiagnosisSubmit;