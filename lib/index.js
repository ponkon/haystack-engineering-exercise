import _ from 'lodash';
import express from 'express';
import sqlite3 from 'sqlite3';
import Promise from 'bluebird';
import bodyParser from 'body-parser';

const app = express();

const sqlite = sqlite3.verbose()
const jsonParser = bodyParser.json();
const db = Promise.promisifyAll(new sqlite.Database('./openmrs.db'));

const allowedPatientSearchKeys = [
  'cd4'
];

const allowedPatientSearchOperators = [
  '<'
];

class QueryBuilderError extends Error {
  constructor (message, status) {
    super(message);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
    this.status = status || 400;   
  }
}

const errorHandler = (err, req, res, next) => {
  if (err.name == QueryBuilderError) {
    res.status(err.status).send(err.message);
  }
}

const searchPatientQueryBuilder = (key, operator) => {

  const foundKey = allowedPatientSearchKeys.find((element) => key == element);
  const foundOperator = allowedPatientSearchOperators.find((element) => operator == element);

  if (!foundKey) {
    throw new QueryBuilderError("Unsupported Search Key");
  } else if (!foundOperator) {
    throw new QueryBuilderError("Unsupported Search Operator");
  } else if (foundKey && foundOperator) {
    return `
      select pat.mrn from patient pat
      inner join encounter enc
      on pat.id = enc.patient_id
      inner join lab_result lr
      on lr.encounter_id = enc.id
      where lr.${foundKey} ${foundOperator} ?;
    `;
  }
}

const handleGetEncounters = (req, res, next) => {
  const sql = 'SELECT id, encounter_datetime, patient_id FROM encounter;'

  db.allAsync(sql)
  .then((rows) => {
    const encounters = _.map(rows, (row) => {
      return {
        id: row.id,
        date: row.encounter_datetime,
        patientId: row.patient_id
      };
    })
    res.send(encounters);
  })
  .catch(next)
}

const handleGetEncounterDiagnosisDescription = (req, res, next) => {
  const sql = `
    SELECT diag.name 
    FROM encounter enc
    LEFT JOIN encounter_diagnosis encd
    ON enc.id = encd.encounter_id
    LEFT JOIN diagnosis diag
    ON diag.id = encd.diagnosis_id
    WHERE enc.id = ?;
  `;

  db.getAsync(sql, req.params.encounterId)
  .then((row) => {
    if (!!row && !!row.name) {
      res.send(row.name);
    } else if (!!row && !row.name) {
      res.send('NONE');
    } else {
      res.sendStatus(404);
    }
  })
  .catch(next)
}

const handlePatientSearch = (req, res, next) => {
  const searchKey = req.body.key;
  const searchValue = +req.body.value;
  const searchOperator = req.body.operator;

  const sql = searchPatientQueryBuilder(searchKey, searchOperator);

  db.allAsync(sql, searchValue)
  .then((rows) => {
    if (!!rows && !!rows.length) {
      const patientMRNs = _.map(rows, (row) => row.mrn);
      res.send(patientMRNs);
    } else {
      res.sendStatus(404);
    }
  })
  .catch(next)
}

const handleSendIndexHTML = (req, res, next) => {
  res.sendFile('index.html', {root: __dirname});
}

const handleSendAppJS = (req, res, next) => {
  res.sendFile('app.js', {root: __dirname});
}

const handleSendAppCSS = (req, res, next) => {
  res.sendFile('style.css', {root: __dirname});
}

app.use(jsonParser);
app.get('/', handleSendIndexHTML);
app.get('/app.js', handleSendAppJS);
app.get('/style.css', handleSendAppCSS);
app.get('/encounters', handleGetEncounters);
app.get('/encounters/:encounterId/diagnosis', handleGetEncounterDiagnosisDescription);
app.post('/patients/search', handlePatientSearch);

if(!module.parent){ 
    app.listen(8000); 
}