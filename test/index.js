import hee from '../lib/index';
import rp from 'request-promise';
import _ from 'lodash';

import { expect } from 'chai';

// Should probably create an explicit test database to use
// for now it'll be assumed that the openmrs.db IS the test db.

describe("Haystack Engineering Excersizes", () => {
  describe("Excersize 1: Get All Encounters", () => {
    it("returns 14316 encounters", (done) => {
      const expectedNumEncounters = 14316;
      const reqOptions = {
        uri: 'http://localhost:8000/encounters',
        json: true
      };

      rp(reqOptions)
      .then((res) => {
        expect(res.length).to.deep.equal(expectedNumEncounters);
        done();
      })
    });
    it("returns an array with the first object being known", (done) => {
      const expectedFirstEncounter = { 
        id: 2, 
        date: '2006-02-07 00:00:00.0', 
        patientId: 1866 
      };
      const reqOptions = {
        uri: 'http://localhost:8000/encounters',
        json: true
      };

      rp(reqOptions)
      .then((res) => {
        const firstEncounter = res[0];
        expect(firstEncounter).to.deep.equal(expectedFirstEncounter);
        done();
      })
    });
  });

  describe("Excersize 2: Diagnosis Description", () => {
    it("returns 'ANEMIA' for encounter id 48842", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/encounters/48842/diagnosis'
      };

      rp(reqOptions)
      .then((res) => {
        expect(res).to.equal('ANEMIA');
        done();
      })
    });
    it("returns 'NONE' for encounter id 2", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/encounters/2/diagnosis'
      };

      rp(reqOptions)
      .then((res) => {
        expect(res).to.equal('NONE');
        done();
      })
    });
    it("returns a 404 error when no description for the encounter is found", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/encounters/99999999999/diagnosis'
      };

      rp(reqOptions)
      .catch((err) => {
        expect(err.statusCode).to.equal(404);
        done();
      })
    });
  });

  describe("Excersize 3: CD4 Count Less Than Search", () => {
    it("returns X patients when CD4 Counter less than 100 is POSTed", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/patients/search',
        method: 'POST',
        body: {
          key: 'cd4',
          value: 100,
          operator: '<'
        },
        json: true
      };

      rp(reqOptions)
      .then((res) => {
        expect(res.length).to.equal(104);
        done();
      })
    });
    it("returns MRN001362 for first record when CD4 Counter less than 10 is POSTed", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/patients/search',
        method: 'POST',
        body: {
          key: 'cd4',
          value: 10,
          operator: '<'
        },
        json: true
      };

      rp(reqOptions)
      .then((res) => {
        expect(res[0]).to.equal('MRN001362');
        done();
      })
    });
    it("returns a 404 error when no patients are found", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/patients/search',
        method: 'POST',
        body: {
          key: 'cd4',
          value: -10,
          operator: '<'
        },
        json: true
      };

      rp(reqOptions)
      .catch((err) => {
        expect(err.statusCode).to.equal(404);
        done();
      })
    });
    it("returns a 400 error when an unsupported search operator is sent in the POST body", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/patients/search',
        method: 'POST',
        body: {
          key: 'cd4',
          value: 100,
          operator: "Robert'); SELECT * FROM Students;--"
        },
        json: true
      };

      rp(reqOptions)
      .catch((err) => {
        expect(err.statusCode).to.equal(400);
        done();
      })
    });
    it("returns a 400 error when an unsupported search key is sent in the POST body", (done) => {
      const reqOptions = {
        uri: 'http://localhost:8000/patients/search',
        method: 'POST',
        body: {
          key: "Robert'); SELECT * FROM Students;--",
          value: 100,
          operator: "<"
        },
        json: true
      };

      rp(reqOptions)
      .catch((err) => {
        expect(err.statusCode).to.equal(400);
        done();
      })
    });
  });
});